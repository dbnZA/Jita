// <copyright file="Expr.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

open System
open FsCheck
open NUnit.Framework
open FsCheck.NUnit
open Jita.Math

module ExprTests =
    let addSubtractConstant (v1, x1) (v2, x2) c =
        let elements =
            [ Variable v1, x1, false; Variable v2, x2, false; Constant c, c, c <> 0. ]

        let operators = [ (+), (+); (-), (-) ]

        [ for e1, y1, hasConstant1 in elements do
              for e2, y2, hasConstant2 in elements do
                  for exprOp, valueOp in operators do
                      let e = exprOp e1 e2
                      let y = valueOp y1 y2

                      e,
                      y,
                      match e with
                      | Constant 0. -> false
                      | _ -> hasConstant1 || hasConstant2 ]

    let hasConstant absorb =
        function
        | Product(c, _) -> c <> 1.
        | Sum(c, _) -> c <> 0.
        | Constant c -> c <> absorb
        | _ -> false

    let (==) left right =
        left.Equals(right) || abs (left - right) < 1e-14

    let ``simplify operation``
        (x1: NormalFloat)
        (x2: NormalFloat)
        (x3: NormalFloat)
        (x4: NormalFloat)
        (c1: NormalFloat)
        (c2: NormalFloat)
        exprOp
        valueOp
        op
        =
        let expected = "Expected"
        let unexpected = "Unexpected"
        let x1, x2, x3, x4 = x1.Get, x2.Get, x3.Get, x4.Get

        [ for e1, y1, hasConstant1 in addSubtractConstant ("x1", x1) ("x2", x2) c1.Get do
              for e2, y2, hasConstant2 in addSubtractConstant ("x3", x3) ("x4", x4) c2.Get do
                  let e = exprOp e1 e2
                  let y = valueOp y1 y2
                  let expectConstant = hasConstant1 || hasConstant2

                  let actualY =
                      e |> Expr.evaluate ([ "x1", x1; "x2", x2; "x3", x3; "x4", x4 ] |> Map.ofList)

                  actualY == y
                  |@ $"Invalid simplification: {e |> Expr.format FSharp} <> ({e1 |> Expr.format FSharp}) {op} ({e2 |> Expr.format FSharp}) <> {y} [res: {actualY - y}]"
                  .&. (expectConstant = hasConstant 0. e
                       |@ $"{if expectConstant then expected else unexpected} constant in expression: {e |> Expr.format FSharp} = ({e1 |> Expr.format FSharp}) {op} ({e2 |> Expr.format FSharp})") ]
        |> List.reduce (.&.)

    [<Property>]
    let ``simplify add 0.`` a =
        Expr.Zero + a |> same a
        .&. ((match a with
              | Constant -0. -> a
              | _ -> a + Expr.Zero)
             |> same a)

    [<Property>]
    let ``simplify add`` x1 x2 x3 x4 c1 c2 =
        ``simplify operation`` x1 x2 x3 x4 c1 c2 (+) (+) "+"

    [<Property>]
    let ``simplify divide 0.`` a = Expr.Zero / a |> equals Expr.Zero

    [<Property>]
    let ``simplify divide by 1.`` a = a / Expr.One |> same a

    [<Property>]
    let ``simplify multiply by 0.`` a =
        Expr.Zero * a |> equals Expr.Zero .&. (a * Expr.Zero |> equals Expr.Zero)

    [<Property>]
    let ``simplify multiply by 1.`` a =
        Expr.One * a |> same a .&. (a * Expr.One |> same a)

    [<Property>]
    let ``simplify subtract 0.`` a =
        a - Expr.Zero |> same a
        .&. (Expr.Zero - a |> Expr.equals -a
             |@ $"0. - {Expr.format FSharp (Expr.Zero - a)} <> {Expr.format FSharp (-a)}")

    [<Property>]
    let ``simplify subtract`` x1 x2 x3 x4 c1 c2 =
        ``simplify operation`` x1 x2 x3 x4 c1 c2 (-) (-) "-"

    [<Property>]
    let ``simplify power of 1.`` a = Expr.One ** a |> equals Expr.One

    [<Property>]
    let ``simplify power to 0.`` (a: Expr) = a ** Expr.Zero |> equals Expr.One

    [<Property>]
    let ``simplify mod 0`` (a: Expr) =
        match a with
        | IsConstant 0. -> Expr.Zero % a |> equals Expr.NaN
        | a -> Expr.Zero % a |> equals Expr.Zero

    let ``evaluate binary operator`` exprOp floatOp (a, b) =
        let variables = Map.ofList (a.Variables @ b.Variables)

        Map.count variables <> List.length a.Variables + List.length b.Variables
        .|. (exprOp b.Expression a.Expression
             |> Expr.evaluate variables
             |> approx 7 (floatOp b.Answer a.Answer))

    let ``evaluate unary operator`` exprOp floatOp a =
        exprOp a.Expression
        |> Expr.evaluate (Map.ofList a.Variables)
        |> approx 6 (floatOp a.Answer)

    [<Property(Arbitrary = [| typeof<EvaluationTwoGenerator> |], EndSize = 16, MaxTest = 1000)>]
    let ``evaluate add`` (a, b) =
        ``evaluate binary operator`` (+) (+) (a, b)

    [<Property(Arbitrary = [| typeof<EvaluationTwoGenerator> |], EndSize = 16, MaxTest = 1000)>]
    let ``evaluate divide`` (a, b) =
        ``evaluate binary operator`` (/) (/) (a, b)

    [<Property(Arbitrary = [| typeof<EvaluationTwoGenerator> |], EndSize = 16, MaxTest = 1000)>]
    let ``evaluate multiply`` (a, b) =
        ``evaluate binary operator`` (*) (*) (a, b)

    [<Property(Arbitrary = [| typeof<EvaluationTwoGenerator> |], EndSize = 16, MaxTest = 1000)>]
    let ``evaluate subtract`` (a, b) =
        ``evaluate binary operator`` (-) (-) (a, b)

    [<Property(Arbitrary = [| typeof<EvaluationTwoGenerator> |], EndSize = 16, MaxTest = 1000)>]
    let ``evaluate min`` (a, b) =
        ``evaluate binary operator`` min min (a, b)

    [<Property(Arbitrary = [| typeof<EvaluationTwoGenerator> |], EndSize = 16, MaxTest = 1000)>]
    let ``evaluate max`` (a, b) =
        ``evaluate binary operator`` max max (a, b)

    [<Property(Arbitrary = [| typeof<EvaluationTwoGenerator> |], EndSize = 16, MaxTest = 1000)>]
    let ``evaluate pow`` (a, b) =
        ``evaluate binary operator`` ( ** ) ( ** ) (a, b)

    [<Property(Arbitrary = [| typeof<EvaluationGenerator> |], EndSize = 16, MaxTest = 1000)>]
    let ``evaluate negate`` a = ``evaluate unary operator`` (~-) (~-) a

    [<Property(Arbitrary = [| typeof<EvaluationGenerator> |], EndSize = 16, MaxTest = 1000)>]
    let ``evaluate abs`` a = ``evaluate unary operator`` abs abs a

    [<Property(Arbitrary = [| typeof<EvaluationGenerator> |], EndSize = 16, MaxTest = 1000)>]
    let ``evaluate exp`` a = ``evaluate unary operator`` exp exp a

    [<Property(Arbitrary = [| typeof<EvaluationGenerator> |], EndSize = 16, MaxTest = 1000)>]
    let ``evaluate log`` a = ``evaluate unary operator`` log log a

    [<Property(Arbitrary = [| typeof<EvaluationGenerator> |], EndSize = 16, MaxTest = 1000)>]
    let ``evaluate sqrt`` a = ``evaluate unary operator`` sqrt sqrt a

    [<Property(Arbitrary = [| typeof<EvaluationGenerator> |], EndSize = 16, MaxTest = 1000)>]
    let ``evaluate mod`` (a, b) =
        ``evaluate binary operator`` (%) (%) (a, b)

    [<Property(Arbitrary = [| typeof<EvaluationGenerator> |], EndSize = 16, MaxTest = 1000)>]
    let ``evaluate copysign`` (a, b) =
        ``evaluate binary operator`` Expr.CopySign (fun v s -> Double.CopySign(v, s)) (a, b)

    [<Test>]
    let ``equals float`` () =
        let expr = Constant 1. :> IEquatable<double>
        Assert.That(expr.Equals(1.), Is.True)
        Assert.That(expr.Equals(2.), Is.False)

    [<Test>]
    let ``equals string`` () =
        let expr = Variable "a" :> IEquatable<string>
        Assert.That(expr.Equals("a"), Is.True)
        Assert.That(expr.Equals("b"), Is.False)

    [<Property>]
    let ``compare float`` (a: NormalFloat) (b: NormalFloat) =
        let expected = compare a.Get b.Get
        let a = Constant a.Get
        let b = Constant b.Get

        (compare a b |> equals expected)
        .&. (compare (Annotation(a, a)) b |> equals expected)
        .&. (compare a (Annotation(b, b)) |> equals expected)
        .&. (compare (Annotation(a, a)) (Annotation(b, b)) |> equals expected)

    [<Property>]
    let ``compare string`` a b =
        compare (Variable a) (Variable b) |> equals (compare a b)

    [<Test>]
    let ``bad comparision`` () =
        Assert.Throws<ArgumentException>(fun () -> compare (Constant 1.) (Variable "a") |> ignore)
        |> ignore

        Assert.Throws<ArgumentException>(fun () -> (Constant 1. :> IComparable).CompareTo("a") |> ignore)
        |> ignore

// <copyright file="Msil.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

open NUnit.Framework
open FsCheck
open FsCheck.NUnit

module MsilTests =
    [<Property>]
    let ``compile variable`` xValue =
        let x = Variable "x"
        let expr = Constant 2. * x + Expr.One
        let yValue = 2. * xValue + 1.
        [| expr |] |> msil [| "x" |] <| [| xValue |] |> Array.item 0 |> equals yValue

    [<Property(MaxTest = 1)>]
    let ``optimise duplicate expression`` () =
        let depth = 1 <<< 4

        let rec dup count tail expr =
            if count = 0 then
                tail |> List.reduce (fun e1 e2 -> BinaryFunction(Add, e1, e2))
            else
                BinaryFunction(Add, expr, expr) |> dup (count - 1) (expr :: tail)

        let expr = Variable "x" |> dup depth []
        let expected = 2. ** (float depth) - 1.
        [| expr |] |> msil [| "x" |] <| [| 1. |] |> Array.item 0 |> equals expected

    [<Property(MaxTest = 1)>]
    let ``tail call`` () =
        let x = Variable "x" + Variable "x"

        ([| x; x |] |> msil [| "x" |] <| [| 1. |] = [| 2.; 2. |]) |@ "n = 2 tail call"
        .&. ([| x; x; x |] |> msil [| "x" |] <| [| 1. |] = [| 2.; 2.; 2. |])
        |@ "n = n + 1 tail call"

    [<Test>]
    let ``method size exceedance`` () =
        let depth = 1 <<< 16

        let rec dup count tail expr =
            if count = 0 then
                tail |> List.rev |> List.toArray
            else
                dup (count - 1) (expr :: tail) (BinaryFunction(Add, expr, expr))

        let exprs = Variable "x" |> dup depth []
        let expected = [| for d in 0 .. depth - 1 -> 2. ** (float d) |]

        exprs |> msil [| "x" |] <| [| 1. |]
        |> Array.zip expected
        |> Array.iter (fun (x, y) -> Assert.That(y, Is.EqualTo(x)))

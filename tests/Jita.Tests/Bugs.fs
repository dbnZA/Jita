// <copyright file="Expr.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

open FsCheck
open NUnit.Framework

module Bugs =
    [<Test>]
    let ``#34`` () =
        let a = Variable("a")
        Assert.That((a / (a * a)) |> Expr.evaluate (Map.ofList [ "a", 4. ]), Is.EqualTo(0.25))

    [<Test>]
    let ``fuzzing: evaluate expression [StdGen [713830221, 297327816]]`` () =
        { Expression = Root(Variable "A", 0)
          Answer = nan
          Variables = [ ("A", 1.) ] }
        |> EvaluateTests.``evaluate expression``
        |> Check.QuickThrowOnFailure

    [<Test>]
    let ``fuzzing: evaluate expression copysign + hypot`` () =
        { Expression = BinaryFunction(CopySign, Expr.One, BinaryFunction(Hypotenuse, Expr.NaN, Expr.Zero))
          Answer = -1.
          Variables = [ "", 0. ] }
        |> EvaluateTests.``evaluate expression``
        |> Check.QuickThrowOnFailure

    [<Test>]
    let ``fuzzing: evaluate expression copysign + product + root`` () =
        { Expression = BinaryFunction(CopySign, Expr.One, Product(1., [ NonInverse(Root(Expr.NegativeOne, 2)) ]))
          Answer = -1.
          Variables = [ "", 0 ] }
        |> EvaluateTests.``evaluate expression``
        |> Check.QuickThrowOnFailure

    [<Test>]
    let ``fuzzing: evaluate expression copysign + product + root + root`` () =
        { Expression =
            BinaryFunction(CopySign, Expr.One, Product(1., [ NonInverse(Root(Root(Expr.NegativeOne, 2), 1)) ]))
          Answer = -1.
          Variables = [ "", 0 ] }
        |> EvaluateTests.``evaluate expression``
        |> Check.QuickThrowOnFailure

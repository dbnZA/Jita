// <copyright file="Compile.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

open FsCheck
open FsCheck.NUnit
open Jita.Math

module CompileTests =
    let ``compiles to`` variables expected expr =
        let symbols = variables |> Array.map fst
        let values = variables |> Array.map snd

        expr |> Expr.evaluate (variables |> Map.ofArray) |> equals expected
        |@ "evaluate"
        .&. ([| expr |] |> msil symbols <| values |> Array.item 0 |> equals expected
             |@ "compile Msil float")
        .&. ([| expr |] |> msil symbols <| toVector values
             |> Array.item 0
             |> Vector.all equals expected
             |@ "compile Msil float Vector")
        .&. ([| expr |] |> llvm symbols <| values |> Array.item 0 |> approx 2 expected
             |@ "compile LLVM float")
        .&. ([| expr |] |> llvm symbols <| toVector values
             |> Array.item 0
             |> Vector.all (approx 2) expected
             |@ "compile LLVM float Vector")

    let ``compile sum/product`` exprOp op (c: NormalFloat) (x: NormalFloat) (y: NormalFloat) =
        let (.&.) e1 e2 = exprOp e1 e2
        let (&.) e1 e2 = op e1 e2
        let c, x, y = c.Get, x.Get, y.Get

        Constant c .&. Variable "x" .&. Variable "y"
        |> ``compiles to`` [| "x", x; "y", y |] (c &. x &. y)

    let ``compile binary function`` exprFunc func (x: NormalFloat) (y: NormalFloat) =
        let x, y = x.Get, y.Get

        exprFunc (Variable "x") (Variable "y")
        |> ``compiles to`` [| "x", x; "y", y |] (func x y)

    let ``compile unary function`` exprFunc func (x: NormalFloat) =
        let x = x.Get
        exprFunc (Variable "x") |> ``compiles to`` [| "x", x |] (func x)

    [<Property>]
    let ``compile addition`` c x y = ``compile sum/product`` (+) (+) c x y

    [<Property>]
    let ``compile divide`` (c: NormalFloat) x y =
        c.Get <> 0. ==> ``compile sum/product`` (/) (/) c x y

    [<Property>]
    let ``compile rootn`` (x: NormalFloat) n =
        let x = x.Get

        rootn (Variable "x") n |> ``compiles to`` [| "x", x |] (rootn x n)

    [<Property>]
    let ``compile hypotenuse`` x y =
        ``compile binary function`` hypot hypot x y

    [<Property>]
    let ``compile max`` x y = ``compile binary function`` max max x y

    [<Property>]
    let ``compile min`` x y = ``compile binary function`` min min x y

    [<Property>]
    let ``compile multiply`` c x y = ``compile sum/product`` (*) (*) c x y

    [<Property>]
    let ``compile power`` x y =
        ``compile binary function`` ( ** ) ( ** ) x y

    [<Property>]
    let ``compile subtract`` c x y = ``compile sum/product`` (-) (-) c x y

    [<Property>]
    let ``compile modulus`` x y = ``compile binary function`` (%) (%) x y

    [<Property>]
    let ``compile copysign`` x y =
        ``compile binary function`` copysign copysign x y

    [<Property>]
    let ``compile absolute`` x = ``compile unary function`` abs abs x

    [<Property>]
    let ``compile cube root`` x = ``compile unary function`` cbrt cbrt x

    [<Property>]
    let ``compile exponential`` x = ``compile unary function`` exp exp x

    [<Property>]
    let ``compile logarithm`` x = ``compile unary function`` log log x

    [<Property>]
    let ``compile negate`` x = ``compile unary function`` (~-) (~-) x

    [<Property>]
    let ``compile square root`` x = ``compile unary function`` sqrt sqrt x

    [<Property>]
    let ``compile variable`` v x =
        Variable v |> ``compiles to`` [| v, x |] x

    [<Property>]
    let ``compile constant`` c =
        Constant c |> ``compiles to`` [| "x", 0. |] c

// <copyright file="ByteCode.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

open System.IO
open NUnit.Framework
open FsCheck
open FsCheck.NUnit

module ByteCodeTests =
    [<Property(Arbitrary = [| typeof<EvaluationGenerator> |], EndSize = 16, MaxTest = 1000)>]
    let ``byte code round trip`` eval =
        [ Exact; Compressed ]
        |> List.map (fun options ->
            use buffer = new MemoryStream()
            [| eval.Expression |] |> Expr.serializeByteCode buffer options
            buffer.Seek(0L, SeekOrigin.Begin) |> ignore
            let expr = Expr.deserializeByteCode buffer |> Array.head

            expr |> Expr.equals eval.Expression
            |@ $"Byte code round trip ({options}): {Expr.format FSharp eval.Expression} <> {Expr.format FSharp expr}")
        |> List.reduce (.&.)

    [<Test>]
    let ``byte code compressing`` () =
        use buffer = new MemoryStream()
        let a = Variable("a")

        [| (Expr.One + a) * (Expr.One + a) |]
        |> Expr.serializeByteCode buffer Compressed

        buffer.Seek(0L, SeekOrigin.Begin) |> ignore

        match Expr.deserializeByteCode buffer with
        | [| Product(1., [ NonInverse e1; NonInverse e2 ]) |] -> Assert.That(e1, Is.SameAs(e2))
        | expr -> Assert.Fail($"Unexpected round trip value: {expr}")

    [<Test>]
    let ``byte code exact`` () =
        use buffer = new MemoryStream()
        let a = Variable("a")
        [| (Expr.One + a) * (Expr.One + a) |] |> Expr.serializeByteCode buffer Exact
        buffer.Seek(0L, SeekOrigin.Begin) |> ignore

        match Expr.deserializeByteCode buffer with
        | [| Product(1., [ NonInverse e1; NonInverse e2 ]) |] -> Assert.That(e1, Is.Not.SameAs(e2))
        | expr -> Assert.Fail($"Unexpected round trip value: {expr}")

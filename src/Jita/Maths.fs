namespace Jita

open System.Numerics

module Math =
    let inline cbrt<'T when IRootFunctions<'T>> x = 'T.Cbrt(x)

    let inline copysign<'T when INumber<'T>> v s = 'T.CopySign(v, s)

    let inline rootn<'T when IRootFunctions<'T>> x n = 'T.RootN(x, n)

    let inline hypot<'T when IRootFunctions<'T>> x y = 'T.Hypot(x, y)

    let inline max<'T when INumber<'T>> x y = 'T.Max(x, y)

    let inline min<'T when INumber<'T>> x y = 'T.Min(x, y)

// <copyright file="Traverse.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

[<AbstractClass>]
type SimpleExprVisitor<'T>() =
    interface IInlineExprVisitor<'T> with
        member this.VisitBinaryFunc op e1 e2 = this.VisitBinaryFunc op e1 e2

        member this.VisitConstant c = this.VisitConstant c

        member this.VisitProduct c s =
            let rec visitProduct' c =
                function
                | NonInverse e :: s -> visitProduct' (this.VisitBinaryFunc Multiply c e) s
                | Inverse e :: s -> visitProduct' (this.VisitBinaryFunc Divide c e) s
                | [] -> c

            match s |> List.rev with
            | NonInverse e :: s when c = 1. -> visitProduct' e s
            | _ :: _ -> visitProduct' (this.VisitConstant c) s
            | [] -> this.VisitConstant 1.

        member this.VisitInlineProduct e1 e2 =
            match e2 with
            | NonInverse e2 -> this.VisitBinaryFunc Multiply e1 e2
            | Inverse e2 -> this.VisitBinaryFunc Divide e1 e2

        member this.VisitRoot e n = this.VisitRoot e n

        member this.VisitSum c s =
            let rec visitSum' c =
                function
                | NonInverse e :: s -> visitSum' (this.VisitBinaryFunc Add c e) s
                | Inverse e :: s -> visitSum' (this.VisitBinaryFunc Subtract c e) s
                | [] -> c

            match s |> List.rev with
            | NonInverse e :: s when c = 0. -> visitSum' e s
            | Inverse e :: s when c = 0. -> visitSum' (this.VisitUnaryFunc Negate e) s
            | _ :: _ -> visitSum' (this.VisitConstant c) s
            | [] -> this.VisitConstant 0.

        member this.VisitInlineSum e1 e2 =
            match e2 with
            | NonInverse e2 -> this.VisitBinaryFunc Add e1 e2
            | Inverse e2 -> this.VisitBinaryFunc Subtract e1 e2

        member this.VisitUnaryFunc op e = this.VisitUnaryFunc op e

        member this.VisitVariable v = this.VisitVariable v

    abstract VisitBinaryFunc: BinaryFunction -> 'T -> 'T -> 'T

    abstract VisitConstant: double -> 'T

    abstract VisitRoot: 'T -> int -> 'T

    abstract VisitUnaryFunc: UnaryFunction -> 'T -> 'T

    abstract VisitVariable: string -> 'T

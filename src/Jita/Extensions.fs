// <copyright file="Extensions" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

open System
open System.Runtime.CompilerServices

[<AbstractClass; Sealed>]
[<Extension>]
type Extensions =
    [<Extension>]
    static member Serialize(this, stream, ?options) =
        this |> Expr.serializeByteCode stream (defaultArg options Compressed)

    [<Extension>]
    static member DeserializeExpr(stream) = Expr.deserializeByteCode stream

    [<Extension>]
    static member CompileMsil<'T>(this, [<ParamArray>] variables) = this |> Expr.compileMsil<'T> variables

    [<Extension>]
    static member Deduplicate(this) = this |> Expr.deduplicateTree

    [<Extension>]
    static member Deduplicate(this) = this |> Expr.deduplicateTrees

    [<Extension>]
    static member Evaluate(this, variables) =
        this |> Expr.evaluate (variables |> Map.ofSeq)

    [<Extension>]
    static member Format(this, style) = this |> Expr.format style

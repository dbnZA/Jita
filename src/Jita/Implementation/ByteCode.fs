// <copyright file="ByteCode.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita.Implementation

open System
open System.Collections
open System.Collections.Generic
open System.IO
open System.Runtime.InteropServices
open System.Text
open FSharp.NativeInterop
open Jita
open Jita.Private

#nowarn "9"

[<AutoOpen>]
module internal ByteCode =
    type private OpCodeLength =
        | OpCodeMask = 0b11100000uy
        | LengthMask = 0b00011111uy
        | MaxValue = 0b00011100uy
        | Bits8 = 0b00011101uy
        | Bits16 = 0b00011110uy
        | Bits32 = 0b00011111uy

    let private bits8MaxValue = (int Byte.MaxValue + 1) + int OpCodeLength.MaxValue

    let private bits16MaxValue = (int Byte.MaxValue + 1) + bits8MaxValue

    let private referenceCacheThreshold = int OpCodeLength.MaxValue

    type private OpCode =
        | ConstantFloat64 = 0x00uy
        | Add = 0x01uy
        | Divide = 0x02uy
        | Max = 0x03uy
        | Min = 0x04uy
        | Multiply = 0x05uy
        | Power = 0x06uy
        | Subtract = 0x07uy
        | Modulus = 0x08uy
        | CopySign = 0x09uy
        | Hypotenuse = 0x0auy
        | VariableNull = 0x10uy
        | Absolute = 0x11uy
        | Exponent = 0x12uy
        | Logarithm = 0x13uy
        | Negate = 0x14uy
        | SquareRoot = 0x15uy
        | CubeRoot = 0x16uy
        | Root = 0x20uy
        | ProductOne = 0x40uy
        | Product = 0x60uy
        | SumZero = 0x80uy
        | Sum = 0xA0uy
        | Variable = 0xC0uy
        | Reference = 0xE0uy

    type private BufferedWriteStream(stream: Stream) =
        [<Literal>]
        let Length = 65536

        let mutable consumed = 0

        let buffer = Array.zeroCreate Length

        let flush () =
            stream.Write(buffer, 0, consumed)
            consumed <- 0

        let ensure size =
            if consumed + size > Length then
                flush ()

        member _.Write(array: byte array) =
            if array.Length > Length then
                failwith $"Buffered request of {array.Length} bytes exceeds capacity of {Length} bytes"

            ensure array.Length
            array.CopyTo(buffer, consumed)
            consumed <- consumed + array.Length

        member _.Write<'a when 'a :> ValueType and 'a: (new: unit -> 'a) and 'a: struct>(value: 'a) =
            ensure sizeof<'a>
            MemoryMarshal.Cast(buffer.AsSpan(consumed, sizeof<'a>)).[0] <- value
            consumed <- consumed + sizeof<'a>

        interface IDisposable with
            member _.Dispose() =
                if consumed > 0 then
                    flush ()

    type private BufferedReadStream(stream: Stream) =
        [<Literal>]
        let Length = 65536

        let mutable consumed = 0

        let mutable available = 0

        let buffer = Array.zeroCreate Length

        let read () =
            let unconsumed =
                if consumed < available then
                    let unconsumed = available - consumed

                    let carryOverBuffer =
                        Span<byte>(NativePtr.stackalloc<byte> unconsumed |> NativePtr.toVoidPtr, unconsumed)

                    buffer[consumed .. (available - 1)].AsSpan().CopyTo(carryOverBuffer)
                    carryOverBuffer.CopyTo(buffer.AsSpan())
                    unconsumed
                else
                    0

            available <- stream.Read(buffer, unconsumed, Length - unconsumed) + unconsumed
            consumed <- 0

        let ensure size =
            if consumed + size > available then
                read ()

                if size > available && available > 0 then
                    failwith $"Unable to fill buffer with {size} bytes"
                else
                    available <> 0
            else
                true

        let require size =
            if not (ensure size) then
                failwith $"Unable to fill buffer with {size} bytes"

        member _.Read(size) =
            if size > Length then
                failwith $"Buffered request of {size} bytes exceeds capacity of {Length} bytes"

            require size
            let array = buffer[consumed .. (consumed + size - 1)]
            consumed <- consumed + size
            array

        member _.TryReadByte() =
            if ensure 1 then
                let byte = buffer[consumed]
                consumed <- consumed + 1
                ValueSome byte
            else
                ValueNone

        member _.Read<'a when 'a :> ValueType and 'a: (new: unit -> 'a) and 'a: struct>() =
            require sizeof<'a>
            let value = MemoryMarshal.Cast<byte, 'a>(buffer.AsSpan(consumed, sizeof<'a>)).[0]
            consumed <- consumed + sizeof<'a>
            value

    let serializeByteCode (stream: Stream) options exprs =
        use stream = new BufferedWriteStream(stream)

        let comparer =
            match options with
            | Exact -> PhysicalEqualityComparer() :> IEqualityComparer<Expr>
            | Compressed -> ExprComparer(exprs)

        let cache = Dictionary<Expr, int>(comparer)
        let mutable cacheIndex = 0

        let writeLength (opCode: OpCode) length =
            let opCode = uint8 opCode

            if length <= int OpCodeLength.MaxValue then
                stream.Write(opCode ||| uint8 length)
            elif length <= bits8MaxValue then
                stream.Write(opCode ||| uint8 OpCodeLength.Bits8)
                stream.Write(uint8 (length - int OpCodeLength.MaxValue - 1))
            elif length <= bits16MaxValue then
                stream.Write(opCode ||| uint8 OpCodeLength.Bits16)
                stream.Write(uint16 (length - bits8MaxValue - 1))
            else
                stream.Write(opCode ||| uint8 OpCodeLength.Bits32)
                stream.Write(length)

        let rec writeFuncN absorb absorbOpCode opCode (c: float, s) =
            let length = s |> List.length
            writeLength (if c = absorb then absorbOpCode else opCode) length

            if c <> absorb then
                visitConstant c

            if length > 0 then
                let bits = BitArray(length)

                let rec writeInvertible' i =
                    function
                    | NonInverse _ :: s ->
                        bits[i] <- false
                        writeInvertible' (i + 1) s
                    | Inverse _ :: s ->
                        bits[i] <- true
                        writeInvertible' (i + 1) s
                    | [] ->
                        let bytes = Array.zeroCreate ((length - 1) / sizeof<uint8> + 1)
                        bits.CopyTo(bytes, 0)
                        stream.Write(bytes)

                writeInvertible' 0 s

            true

        and visitConstant c =
            let expr = Constant c

            match visitor.LeadFilter expr with
            | None ->
                if visitor.Callback.VisitConstant c then
                    visitor.TailFilter expr true |> ignore
            | Some _ -> ()

        and visitor =
            { Callback =
                { new IExprVisitor<bool> with
                    member _.VisitBinaryFunc op _ _ =
                        stream.Write(
                            match op with
                            | Add -> OpCode.Add
                            | CopySign -> OpCode.CopySign
                            | Divide -> OpCode.Divide
                            | Hypotenuse -> OpCode.Hypotenuse
                            | Max -> OpCode.Max
                            | Min -> OpCode.Min
                            | Modulus -> OpCode.Modulus
                            | Multiply -> OpCode.Multiply
                            | Power -> OpCode.Power
                            | Subtract -> OpCode.Subtract
                        )

                        true

                    member _.VisitConstant c =
                        stream.Write(OpCode.ConstantFloat64)
                        stream.Write(c)
                        true

                    member this.VisitProduct c s =
                        writeFuncN 1. OpCode.ProductOne OpCode.Product (c, s)

                    member this.VisitRoot _ n =
                        stream.Write(OpCode.Root)
                        stream.Write(n)
                        true

                    member this.VisitSum c s =
                        writeFuncN 0. OpCode.SumZero OpCode.Sum (c, s)

                    member _.VisitUnaryFunc op _ =
                        stream.Write(
                            match op with
                            | Absolute -> OpCode.Absolute
                            | CubeRoot -> OpCode.CubeRoot
                            | Exponent -> OpCode.Exponent
                            | Logarithm -> OpCode.Logarithm
                            | Negate -> OpCode.Negate
                            | SquareRoot -> OpCode.SquareRoot
                        )

                        true

                    member _.VisitVariable v =
                        if isNull v then
                            stream.Write(OpCode.VariableNull)
                            false
                        elif v = "" then
                            writeLength OpCode.Variable 0
                            false
                        else
                            let v = Encoding.UTF8.GetBytes(v)
                            writeLength OpCode.Variable v.Length
                            stream.Write(v)
                            true }
              LeadFilter =
                fun expr ->
                    match cache.TryGetValue(expr) with
                    | true, index ->
                        let offset = cacheIndex - index - 1
                        writeLength OpCode.Reference offset

                        if offset > referenceCacheThreshold then
                            cache[expr] <- cacheIndex
                            cacheIndex <- cacheIndex + 1

                        Some false
                    | false, _ -> None
              TailFilter =
                fun expr cacheable ->
                    if cacheable then
                        cache.Add(expr, cacheIndex)
                        cacheIndex <- cacheIndex + 1

                    false }

        exprs |> Array.iter (ExprComparer.Traverse visitor >> ignore)

    let deserializeByteCode (stream: Stream) =
        let stream = BufferedReadStream(stream)
        let cache = List<Expr>()

        let readLength opCode =
            match
                LanguagePrimitives.EnumOfValue<uint8, OpCodeLength>(opCode)
                &&& OpCodeLength.LengthMask
            with
            | OpCodeLength.Bits8 -> int (stream.Read<uint8>()) + int OpCodeLength.MaxValue + 1
            | OpCodeLength.Bits16 -> int (stream.Read<uint16>()) + bits8MaxValue + 1
            | OpCodeLength.Bits32 -> stream.Read<int>()
            | length -> int length

        let variableNull = Variable null
        let variableEmpty = Variable ""

        let rec deserialize' stack =
            let binaryExpr op =
                match stack with
                | e2 :: e1 :: stack -> BinaryFunction(op, e1, e2), true, stack
                | _ -> failwith "Required two expressions for binary operation"

            let unaryExpr op =
                match stack with
                | e :: stack -> UnaryFunction(op, e), true, stack
                | [] -> failwith "Required one expression for unary operation"

            let rec funcNExpr expr absorb opCode =
                let length = readLength opCode

                let c, stack =
                    match absorb with
                    | Some c -> c, stack
                    | None ->
                        match parse (stream.Read<uint8>()) with
                        | Constant c :: stack -> c, stack
                        | _ -> failwith "Required a constant at the top of the stack"

                let s, stack = stack |> List.splitAt length

                let s =
                    if length = 0 then
                        List.empty
                    else
                        let bits = BitArray(stream.Read((length - 1) / sizeof<uint8> + 1))
                        s |> List.mapi (fun i s' -> if bits[i] then Inverse s' else NonInverse s')

                expr (c, s), true, stack

            and parse opCode =
                let expr, cacheable, stack =
                    let lengthOpCode = opCode &&& uint8 OpCodeLength.OpCodeMask

                    match
                        LanguagePrimitives.EnumOfValue<uint8, OpCode>(
                            if lengthOpCode <> 0uy then lengthOpCode else opCode
                        )
                    with
                    | OpCode.ConstantFloat64 -> Constant(stream.Read<float>()), true, stack
                    | OpCode.Add -> binaryExpr Add
                    | OpCode.Divide -> binaryExpr Divide
                    | OpCode.Max -> binaryExpr Max
                    | OpCode.Min -> binaryExpr Min
                    | OpCode.Multiply -> binaryExpr Multiply
                    | OpCode.Power -> binaryExpr Power
                    | OpCode.Subtract -> binaryExpr Subtract
                    | OpCode.Modulus -> binaryExpr Modulus
                    | OpCode.CopySign -> binaryExpr CopySign
                    | OpCode.Hypotenuse -> binaryExpr Hypotenuse
                    | OpCode.VariableNull -> variableNull, false, stack
                    | OpCode.Absolute -> unaryExpr Absolute
                    | OpCode.Exponent -> unaryExpr Exponent
                    | OpCode.Logarithm -> unaryExpr Logarithm
                    | OpCode.Negate -> unaryExpr Negate
                    | OpCode.SquareRoot -> unaryExpr SquareRoot
                    | OpCode.CubeRoot -> unaryExpr CubeRoot
                    | OpCode.Root ->
                        match stack with
                        | e :: stack -> Root(e, stream.Read<int>()), true, stack
                        | [] -> failwith "Required one expression for root operation"
                    | OpCode.ProductOne -> funcNExpr Product (Some 1.) opCode
                    | OpCode.Product -> funcNExpr Product None opCode
                    | OpCode.SumZero -> funcNExpr Sum (Some 0.) opCode
                    | OpCode.Sum -> funcNExpr Sum None opCode
                    | OpCode.Variable ->
                        match readLength opCode with
                        | 0 -> variableEmpty, false, stack
                        | length -> Variable(Encoding.UTF8.GetString(stream.Read(length))), true, stack
                    | OpCode.Reference ->
                        let offset = readLength opCode

                        if offset >= cache.Count then
                            failwith $"Invalid reference: {offset}"

                        cache[cache.Count - offset - 1], offset > referenceCacheThreshold, stack
                    | op -> failwith $"Unknown op-code: %A{op}"

                if cacheable then
                    cache.Add(expr)

                expr :: stack

            match stream.TryReadByte() with
            | ValueSome opCode -> parse opCode |> deserialize'
            | ValueNone -> stack

        deserialize' [] |> Array.ofList |> Array.rev

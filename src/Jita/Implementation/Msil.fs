// <copyright file="Msil.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita.Implementation

open System
open System.Collections.Generic
open System.Numerics
open System.Reflection
open System.Reflection.Emit
open Jita
open Jita.Math
open Jita.Private
open Jita.Private.Emit
open Microsoft.FSharp.NativeInterop

#nowarn "9"

[<CompilationRepresentation(CompilationRepresentationFlags.ModuleSuffix)>]
module internal Msil =
    [<Literal>]
    let private TmpLocal = 0

    [<Literal>]
    let private IlOffsetThreshold = 131072 // 128 KiB

    let inline private map ([<InlineIfLambda>] mapping) (vector: 'a Vector) =
        let n = Vector<'a>.Count
        let buffer = NativePtr.stackalloc n

        for i in 0 .. n - 1 do
            NativePtr.set buffer i (mapping vector[i])

        Vector<'a>(Span<'a>(buffer |> NativePtr.toVoidPtr, n))

    let inline private map2 ([<InlineIfLambda>] mapping) (vector1: 'a Vector) (vector2: 'a Vector) =
        let n = Vector<'a>.Count
        let buffer = NativePtr.stackalloc n

        for i in 0 .. n - 1 do
            NativePtr.set buffer i (mapping vector1[i] vector2[i])

        Vector<'a>(Span<'a>(buffer |> NativePtr.toVoidPtr, n))

    [<AbstractClass; Sealed>]
    type private VectorMath =
        static member Cbrt(value: float Vector) = map cbrt value

        static member Cbrt(value: float32 Vector) = map cbrt value

        static member CopySign(value: float Vector, sign) = map2 copysign value sign

        static member CopySign(value: float32 Vector, sign) = map2 copysign value sign

        static member Exp(value: float32 Vector) = value |> map exp

        static member Exp(value) = value |> map exp

        static member Hypot(x: float Vector, y) = map2 hypot x y

        static member Hypot(x: float32 Vector, y) = map2 hypot x y

        static member Log(value: float32 Vector) = value |> map log

        static member Log(value) = value |> map log

        static member Mod(value: float32 Vector, modulus) = map2 (%) value modulus

        static member Mod(value: float Vector, modulus) = map2 (%) value modulus

        static member RootN(value: float Vector, n) = value |> map (fun x -> rootn x n)

        static member RootN(value: float32 Vector, n) = value |> map (fun x -> rootn x n)

        static member Pow(value: float32 Vector, power) = map2 ( ** ) value power

        static member Pow(value, power) = map2 ( ** ) value power

        static member GetMethodInfo<'a when 'a: struct and 'a: (new: unit -> 'a) and 'a :> ValueType> name =
            typeof<VectorMath>.GetMethods(BindingFlags.NonPublic ||| BindingFlags.Static)
            |> Array.filter (fun m -> m.Name = name && m.GetParameters().[0].ParameterType = typeof<'a Vector>)
            |> Array.exactlyOne

    type private MathModel =
        { Abs: MethodInfo
          Addition: MethodInfo option
          BaseType: Type
          CopySign: MethodInfo
          Cbrt: MethodInfo
          Ctor: ConstructorInfo option
          Division: MethodInfo option
          Exp: MethodInfo
          Hypot: MethodInfo
          Log: MethodInfo
          Max: MethodInfo
          Min: MethodInfo
          Modulus: MethodInfo option
          Multiply: MethodInfo option
          Pow: MethodInfo
          RootN: MethodInfo
          Sqrt: MethodInfo
          Subtraction: MethodInfo option
          UnaryNegation: MethodInfo option }

    let private scalarMath<'a when 'a: struct and 'a: (new: unit -> 'a) and 'a :> ValueType> =
        { Abs = typeof<'a>.GetMethod("Abs")
          Addition = None
          BaseType = typeof<'a>
          CopySign = typeof<'a>.GetMethod("CopySign")
          Cbrt = typeof<'a>.GetMethod("Cbrt")
          Ctor = None
          Division = None
          Exp = typeof<'a>.GetMethod("Exp")
          Hypot = typeof<'a>.GetMethod("Hypot")
          Log = typeof<'a>.GetMethod("Log", [| typeof<'a> |])
          Max = typeof<'a>.GetMethod("Max")
          Min = typeof<'a>.GetMethod("Min")
          Modulus = None
          Multiply = None
          Pow = typeof<'a>.GetMethod("Pow")
          RootN = typeof<'a>.GetMethod("RootN")
          Sqrt = typeof<'a>.GetMethod("Sqrt")
          Subtraction = None
          UnaryNegation = None }

    let private vectorMath<'a when 'a: struct and 'a: (new: unit -> 'a) and 'a :> ValueType> =
        { Abs = typeof<Vector>.GetMethod("Abs").MakeGenericMethod(typeof<'a>)
          Addition = Some(typeof<'a Vector>.GetMethod("op_Addition"))
          BaseType = typeof<'a>
          CopySign = VectorMath.GetMethodInfo<'a> "CopySign"
          Cbrt = VectorMath.GetMethodInfo<'a> "Cbrt"
          Ctor = Some(typeof<'a Vector>.GetConstructor([| typeof<'a> |]))
          Division =
            Some(
                typeof<'a Vector>
                    .GetMethod("op_Division", [| typeof<'a Vector>; typeof<'a Vector> |])
            )
          Exp = VectorMath.GetMethodInfo<'a> "Exp"
          Hypot = VectorMath.GetMethodInfo<'a> "Hypot"
          Log = VectorMath.GetMethodInfo<'a> "Log"
          Max = typeof<Vector>.GetMethod("Max").MakeGenericMethod(typeof<'a>)
          Min = typeof<Vector>.GetMethod("Min").MakeGenericMethod(typeof<'a>)
          Modulus = Some(VectorMath.GetMethodInfo<'a> "Mod")
          Multiply =
            Some(
                typeof<'a Vector>
                    .GetMethod("op_Multiply", [| typeof<'a Vector>; typeof<'a Vector> |])
            )
          Pow = VectorMath.GetMethodInfo<'a> "Pow"
          RootN = VectorMath.GetMethodInfo<'a> "RootN"
          Sqrt = typeof<Vector>.GetMethod("SquareRoot").MakeGenericMethod(typeof<'a>)
          Subtraction = Some(typeof<'a Vector>.GetMethod("op_Subtraction"))
          UnaryNegation = Some(typeof<'a Vector>.GetMethod("op_UnaryNegation")) }

    let private models =
        Dictionary(
            [ yield typeof<float>, scalarMath<float>
              yield typeof<float32>, scalarMath<float32>
              if Vector.IsHardwareAccelerated then
                  yield typeof<float Vector>, vectorMath<float>
                  yield typeof<float32 Vector>, vectorMath<float32> ]
            |> List.map KeyValuePair
        )

    let private callTokenOp methodInfo opCode il =
        match methodInfo with
        | Some methodInfo -> il |> callToken methodInfo
        | None -> il |> opCode

    type private MsilVisitor<'T>(findVariable, il: ILGenerator) =
        inherit SimpleExprVisitor<bool>()

        let model =
            match models.TryGetValue(typeof<'T>) with
            | true, model -> model
            | false, _ -> failwith $"No MSIL compilation model for type '{typeof<'T>.Name}'"

        let abs = il.GetMethodToken(model.Abs)

        let addition = model.Addition |> Option.map il.GetMethodToken

        let cbrt = il.GetMethodToken(model.Cbrt)

        let copySign = il.GetMethodToken(model.CopySign)

        let ctor = model.Ctor |> Option.map il.GetConstructorToken

        let division = model.Division |> Option.map il.GetMethodToken

        let exp = il.GetMethodToken(model.Exp)

        let hypot = il.GetMethodToken(model.Hypot)

        let log = il.GetMethodToken(model.Log)

        let max = il.GetMethodToken(model.Max)

        let min = il.GetMethodToken(model.Min)

        let modulus = model.Modulus |> Option.map il.GetMethodToken

        let multiply = model.Multiply |> Option.map il.GetMethodToken

        let pow = il.GetMethodToken(model.Pow)

        let rootn = il.GetMethodToken(model.RootN)

        let sqrt = il.GetMethodToken(model.Sqrt)

        let subtraction = model.Subtraction |> Option.map il.GetMethodToken

        let unaryNegation = model.UnaryNegation |> Option.map il.GetMethodToken

        override _.VisitBinaryFunc op _ _ =
            match op with
            | Add -> il |> callTokenOp addition add
            | CopySign -> il |> callToken copySign
            | Divide -> il |> callTokenOp division div
            | Hypotenuse -> il |> callToken hypot
            | Max -> il |> callToken max
            | Min -> il |> callToken min
            | Modulus -> il |> callTokenOp modulus rem
            | Multiply -> il |> callTokenOp multiply mul
            | Power -> il |> callToken pow
            | Subtract -> il |> callTokenOp subtraction sub

            true

        override _.VisitConstant c =
            match model.BaseType with
            | t when t = typeof<float32> -> il |> ldc.r4 (float32 c)
            | t when t = typeof<float> -> il |> ldc.r8 c
            | _ -> failwith $"Unsupported type for op code 'ldc': {typeof<'T>.Name}"

            match ctor with
            | Some ctor -> il |> newobjToken ctor
            | None -> ()

            false

        override _.VisitRoot _ n =
            il |> ldc.i4 n
            il |> callToken rootn

            true

        override _.VisitUnaryFunc op _ =
            match op with
            | Absolute -> il |> callToken abs
            | CubeRoot -> il |> callToken cbrt
            | Exponent -> il |> callToken exp
            | Logarithm -> il |> callToken log
            | Negate -> il |> callTokenOp unaryNegation neg
            | SquareRoot -> il |> callToken sqrt

            true

        override _.VisitVariable v =
            let varIndex = findVariable v
            il |> ldarg Compile.InputArg
            il |> ldc.i4 (sizeof<'T> * varIndex)
            il |> add
            il |> ldobj typeof<'T>
            false

    let compile<'a> variables exprs =
        let exprComparer = ExprComparer(exprs)
        let localExpr, localCount = exprs |> Compile.markToLocal exprComparer
        let cachedExpr = HashSet(exprComparer)

        let ptr = typeof<nativeint>

        let createDelegateBuilder () =
            let delegateBuilder = DynamicMethod("JitaMSIL", typeof<Void>, [| ptr; ptr; ptr |])
            let il = delegateBuilder.GetILGenerator()

            if il.DeclareLocal(typeof<'a>).LocalIndex <> TmpLocal then
                failwithf "Temporary must be first local"

            delegateBuilder, il

        let findVariable = Compile.findVariable variables
        let delegateBuilder, mainIl = createDelegateBuilder ()

        let rec compile' i (il: ILGenerator) visitor =
            let visitor =
                match visitor with
                | Some _ -> visitor
                | None -> Some(MsilVisitor<'a>(findVariable, il))

            if il.ILOffset > IlOffsetThreshold then
                let subDelegateBuilder, subIl = createDelegateBuilder ()
                il |> ldarg Compile.InputArg
                il |> ldarg Compile.OutputArg
                il |> ldarg Compile.LocalArg
                il |> tail
                il |> call subDelegateBuilder
                il |> ret
                compile' i subIl None
            else
                exprs[i]
                |> ExprComparer.Traverse
                    { Callback = Option.get visitor
                      LeadFilter =
                        fun expr ->
                            if cachedExpr.Contains(expr) then
                                il |> ldarg Compile.LocalArg
                                il |> ldc.i4 (sizeof<'a> * localExpr[expr])
                                il |> add
                                il |> ldobj typeof<'a>
                                Some false
                            else
                                None
                      TailFilter =
                        fun expr tryCache ->
                            if tryCache then
                                match localExpr.TryGetValue(expr) with
                                | true, index ->
                                    il |> dup
                                    il |> stloc TmpLocal
                                    il |> ldarg Compile.LocalArg
                                    il |> ldc.i4 (sizeof<'a> * index)
                                    il |> add
                                    il |> ldloc TmpLocal
                                    il |> stobj typeof<'a>
                                    cachedExpr.Add(expr) |> ignore
                                | false, _ -> ()

                            false }
                |> ignore

                il |> stloc TmpLocal
                il |> ldarg Compile.OutputArg
                il |> ldc.i4 (sizeof<'a> * i)
                il |> add
                il |> ldloc TmpLocal
                il |> stobj typeof<'a>

                if i <> (exprs |> Array.length) - 1 then
                    compile' (i + 1) il visitor
                else
                    il |> ret

        compile' 0 mainIl None

        ExprFunc<'a>(
            1,
            variables.Length,
            exprs.Length,
            localCount,
            delegateBuilder.CreateDelegate(typeof<Compile.Func>) :?> Compile.Func
        )

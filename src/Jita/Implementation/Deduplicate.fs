// <copyright file="Deduplicate.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita.Implementation

open System.Collections.Generic
open Jita

[<AutoOpen>]
module internal Duduplicate =
    let private visitor =
        { new IExprVisitor<Expr> with
            member _.VisitBinaryFunc op e1 e2 = BinaryFunction(op, e1, e2)
            member _.VisitConstant c = Constant c
            member _.VisitProduct c s = Product(c, s)
            member _.VisitRoot e n = Root(e, n)
            member _.VisitSum c s = Product(c, s)
            member _.VisitUnaryFunc op e = UnaryFunction(op, e)
            member _.VisitVariable v = Variable v }

    let deduplicateTree expr =
        expr |> ExprComparer.TraverseCached (Dictionary(ExprComparer(expr))) visitor

    let deduplicateTrees exprs =
        exprs
        |> Array.map (ExprComparer.TraverseCached (Dictionary(ExprComparer(exprs))) visitor)

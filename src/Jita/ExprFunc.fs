// <copyright file="ExprFunc.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

open System
open System.Runtime.InteropServices

#nowarn "9"
#nowarn "42"

type ExprFunc<'T>(alignment, inputLength, outputLength, bufferLength, func: Compile.Func) =
    do
        if inputLength < 1 then
            invalidArg "inputLength" "Input length must be at least one"

        if outputLength < 1 then
            invalidArg "outputLength" "Output length must be at least one"

        if bufferLength < 0 then
            invalidArg "bufferLength" "Buffer length must be nonnegative"

    let input =
        NativeMemory.AlignedAlloc(unativeint (inputLength * sizeof<'T>), unativeint alignment)

    let output =
        NativeMemory.AlignedAlloc(unativeint (outputLength * sizeof<'T>), unativeint alignment)

    let buffer =
        NativeMemory.AlignedAlloc(unativeint (bufferLength * sizeof<'T>), unativeint alignment)

    member _.Input = Span<'T>(input, inputLength)

    member _.Output = Span<'T>(output, outputLength)

    member _.Copy() =
        ExprFunc(alignment, inputLength, outputLength, bufferLength, func)

    member this.Invoke() =
        let input = (# "" input : byref<'T> #)
        let output = (# "" output : byref<'T> #)
        let buffer = (# "" buffer : byref<'T> #)
        use input = fixed &input
        use output = fixed &output
        use buffer = fixed &buffer

        func.Invoke((# "" input : nativeint #), (# "" output : nativeint #), (# "" buffer : nativeint #))

        this.Output

    override this.Finalize() =
        NativeMemory.Free(input)
        NativeMemory.Free(output)
        NativeMemory.Free(buffer)

// <copyright file="Emit.FSharp.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita.Private

open System
open System.Reflection
open System.Reflection.Emit

module internal Emit =
    let private opCode opCode (il: ILGenerator) = il.Emit(opCode)

    let add il = il |> opCode OpCodes.Add

    let ``and`` il = il |> opCode OpCodes.And

    module beq =
        let s (label: Label) (il: ILGenerator) = il.Emit(OpCodes.Beq_S, label)

    module brfalse =
        let s (label: Label) (il: ILGenerator) = il.Emit(OpCodes.Brfalse, label)

    let call (method: MethodInfo) (il: ILGenerator) = il.Emit(OpCodes.Call, method)

    let callToken (methodToken: int) (il: ILGenerator) = il.Emit(OpCodes.Call, methodToken)

    module conv =
        let i4 il = il |> opCode OpCodes.Conv_I4

    let div il = il |> opCode OpCodes.Div

    let dup il = il |> opCode OpCodes.Dup

    let ldarg arg (il: ILGenerator) =
        match arg with
        | 0 -> il.Emit(OpCodes.Ldarg_0)
        | 1 -> il.Emit(OpCodes.Ldarg_1)
        | 2 -> il.Emit(OpCodes.Ldarg_2)
        | 3 -> il.Emit(OpCodes.Ldarg_3)
        | _ when arg <= 255 -> il.Emit(OpCodes.Ldarg_S, byte arg)
        | _ -> il.Emit(OpCodes.Ldarg, int16 arg)

    let ldarga arg (il: ILGenerator) =
        match arg with
        | _ when arg <= 255 -> il.Emit(OpCodes.Ldarga_S, byte arg)
        | _ -> il.Emit(OpCodes.Ldarga, int16 arg)

    module ldc =
        let i4 value (il: ILGenerator) =
            match value with
            | -1 -> il.Emit(OpCodes.Ldc_I4_M1)
            | 0 -> il.Emit(OpCodes.Ldc_I4_0)
            | 1 -> il.Emit(OpCodes.Ldc_I4_1)
            | 2 -> il.Emit(OpCodes.Ldc_I4_2)
            | 3 -> il.Emit(OpCodes.Ldc_I4_3)
            | 4 -> il.Emit(OpCodes.Ldc_I4_4)
            | 5 -> il.Emit(OpCodes.Ldc_I4_5)
            | 6 -> il.Emit(OpCodes.Ldc_I4_6)
            | 7 -> il.Emit(OpCodes.Ldc_I4_7)
            | 8 -> il.Emit(OpCodes.Ldc_I4_8)
            | _ when -128 <= value && value <= 127 -> il.Emit(OpCodes.Ldc_I4_S, byte value)
            | _ -> il.Emit(OpCodes.Ldc_I4, value)

        let r4 (value: float32) (il: ILGenerator) = il.Emit(OpCodes.Ldc_R4, value)

        let r8 (value: float) (il: ILGenerator) = il.Emit(OpCodes.Ldc_R8, value)

    module ldelem =
        let r4 il = il |> opCode OpCodes.Ldelem_R4

        let r8 il = il |> opCode OpCodes.Ldelem_R8

    let ldelem elementType il =
        match elementType with
        | _ when elementType = typeof<float32> -> il |> ldelem.r4
        | _ when elementType = typeof<float> -> il |> ldelem.r8
        | _ -> il.Emit(OpCodes.Ldelem, elementType)

    let ldelema (elementType: Type) (il: ILGenerator) = il.Emit(OpCodes.Ldelema, elementType)

    let ldlen il = il |> opCode OpCodes.Ldlen

    let ldloc index (il: ILGenerator) =
        match index with
        | 0 -> il.Emit(OpCodes.Ldloc_0)
        | 1 -> il.Emit(OpCodes.Ldloc_1)
        | 2 -> il.Emit(OpCodes.Ldloc_2)
        | 3 -> il.Emit(OpCodes.Ldloc_3)
        | _ when index <= 255 -> il.Emit(OpCodes.Ldloc_S, byte index)
        | _ -> il.Emit(OpCodes.Ldloc, int16 index)

    module ldind =
        let r4 il = il |> opCode OpCodes.Ldind_R4

        let r8 il = il |> opCode OpCodes.Ldind_R8

    let ldobj (objectType: Type) (il: ILGenerator) =
        match objectType with
        | _ when objectType = typeof<float32> -> il |> ldind.r4
        | _ when objectType = typeof<float> -> il |> ldind.r8
        | _ -> il.Emit(OpCodes.Ldobj, objectType)

    let ldstr (str: string) (il: ILGenerator) = il.Emit(OpCodes.Ldstr, str)

    let mul il = il |> opCode OpCodes.Mul

    let neg il = il |> opCode OpCodes.Neg

    let newobj (ctor: ConstructorInfo) (il: ILGenerator) = il.Emit(OpCodes.Newobj, ctor)

    let newobjToken (ctorToken: int) (il: ILGenerator) = il.Emit(OpCodes.Newobj, ctorToken)

    let newarr (elementType: Type) (il: ILGenerator) = il.Emit(OpCodes.Newarr, elementType)

    let pop il = il |> opCode OpCodes.Pop

    let rem il = il |> opCode OpCodes.Rem

    let ret (il: ILGenerator) = il.Emit(OpCodes.Ret)

    module stelem =
        let r4 il = il |> opCode OpCodes.Stelem_R4

        let r8 il = il |> opCode OpCodes.Stelem_R8

    let stelem elementType il =
        match elementType with
        | _ when elementType = typeof<float32> -> il |> stelem.r4
        | _ when elementType = typeof<float> -> il |> stelem.r8
        | _ -> il.Emit(OpCodes.Stelem, elementType)

    let stelema (elementType: Type) (il: ILGenerator) =
        il.Emit(OpCodes.Stelem_Ref, elementType)

    let stloc index (il: ILGenerator) =
        match index with
        | 0 -> il.Emit(OpCodes.Stloc_0)
        | 1 -> il.Emit(OpCodes.Stloc_1)
        | 2 -> il.Emit(OpCodes.Stloc_2)
        | 3 -> il.Emit(OpCodes.Stloc_3)
        | _ when index <= 255 -> il.Emit(OpCodes.Stloc_S, byte index)
        | _ -> il.Emit(OpCodes.Stloc, int16 index)

    module stind =
        let r4 il = il |> opCode OpCodes.Stind_R4

        let r8 il = il |> opCode OpCodes.Stind_R8

        let t<'a> il =
            if typeof<'a> = typeof<float32> then
                il |> r4
            elif typeof<'a> = typeof<float> then
                il |> r8
            else
                failwithf $"Unsupported type for op code 'stind': {typeof<'a>.Name}"

    let stobj (objectType: Type) (il: ILGenerator) =
        match objectType with
        | _ when objectType = typeof<float32> -> il |> stind.r4
        | _ when objectType = typeof<float> -> il |> stind.r8
        | _ -> il.Emit(OpCodes.Stobj, objectType)

    let sub il = il |> opCode OpCodes.Sub

    let tail il = il |> opCode OpCodes.Tailcall

    let throw il = il |> opCode OpCodes.Throw

// <copyright file="HashCode.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita.Private

open System

// https://xxhash.com
// https://github.com/dotnet/coreclr/blob/85374ceaed177f71472cc4c23c69daf7402e5048/src/System.Private.CoreLib/shared/System/HashCode.cs
module internal HashCode =
    let combine2 hash1 hash2 = HashCode.Combine(hash1, hash2)

    let combine3 hash1 hash2 hash3 = HashCode.Combine(hash1, hash2, hash3)

    let empty = HashCode()

    let add t (hashCode: HashCode) =
        hashCode.Add(t)
        hashCode

    let hashCode (hashCode: HashCode) = hashCode.ToHashCode()

    let init t = empty |> add t

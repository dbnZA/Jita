// <copyright file="ILGeneratorExtensions.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita.Private

open System
open System.Reflection
open System.Reflection.Emit
open System.Runtime.CompilerServices

[<Extension>]
[<AbstractClass; Sealed>]
type internal ILGeneratorExtensions() =
    static let ilGeneratorType = typeof<ILGenerator>

    static let runtimeType = ilGeneratorType.Assembly.GetType("System.RuntimeType")

    static let getToken this info (runtimeInfoType: Type) =
        let getRuntimeType =
            runtimeInfoType.GetMethod("GetRuntimeType", BindingFlags.NonPublic ||| BindingFlags.Instance)

        let declaringType = getRuntimeType.Invoke(info, [||]) :?> Type

        if declaringType.IsGenericType then
            let getTokenFor =
                this
                    .GetType()
                    .GetMethod(
                        "GetTokenFor",
                        BindingFlags.NonPublic ||| BindingFlags.Instance,
                        null,
                        [| runtimeInfoType; runtimeType |],
                        null
                    )

            getTokenFor.Invoke(this, [| info; declaringType |]) :?> int
        else
            let getTokenFor =
                this
                    .GetType()
                    .GetMethod(
                        "GetTokenFor",
                        BindingFlags.NonPublic ||| BindingFlags.Instance,
                        null,
                        [| runtimeInfoType |],
                        null
                    )

            getTokenFor.Invoke(this, [| info |]) :?> int

    static let runtimeConstructorInfoType =
        ilGeneratorType.Assembly.GetType("System.Reflection.RuntimeConstructorInfo")

    static let runtimeMethodInfoType =
        ilGeneratorType.Assembly.GetType("System.Reflection.RuntimeMethodInfo")

    [<Extension>]
    static member GetConstructorToken(this: ILGenerator, ctorInfo: ConstructorInfo) =
        getToken this ctorInfo runtimeConstructorInfoType

    [<Extension>]
    static member GetMethodToken(this: ILGenerator, methodInfo: MethodInfo) =
        getToken this methodInfo runtimeMethodInfoType

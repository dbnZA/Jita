// <copyright file="PhysicalEquality.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita.Private

open System.Collections.Generic
open System.Runtime.CompilerServices

[<AutoOpen>]
module internal PhysicalEquality =
    let internal (.=.) left right =
        LanguagePrimitives.PhysicalEquality left right

    type internal PhysicalEqualityComparer<'T when 'T: not struct>() =
        interface IEqualityComparer<'T> with
            member _.Equals(x, y) = x .=. y

            member _.GetHashCode(x) = RuntimeHelpers.GetHashCode(x)

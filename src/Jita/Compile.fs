// <copyright file="Compile.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace Jita

open System.Collections.Generic

[<CompilationRepresentation(CompilationRepresentationFlags.ModuleSuffix)>]
module Compile =
    [<Literal>]
    let InputArg = 0

    [<Literal>]
    let OutputArg = 1

    [<Literal>]
    let LocalArg = 2

    type Func = delegate of nativeint * nativeint * nativeint -> unit

    let markToLocal (exprComparer: IEqualityComparer<Expr>) exprs =
        let marks = Dictionary<Expr, int * int>(exprComparer)

        let visitor =
            { Callback =
                { new SimpleExprVisitor<bool>() with
                    member _.VisitBinaryFunc _ _ _ = true
                    member _.VisitConstant _ = false
                    member _.VisitRoot _ _ = true
                    member _.VisitUnaryFunc _ _ = true
                    member _.VisitVariable _ = false }
              LeadFilter =
                fun expr ->
                    match marks.TryGetValue(expr) with
                    | true, (firstGen, _) ->
                        marks[expr] <- (firstGen, marks.Count)
                        Some false
                    | false, _ -> None
              TailFilter =
                fun expr recordHit ->
                    if recordHit then
                        marks.Add(expr, (marks.Count, marks.Count))

                    false }

        exprs |> Array.iter (fun expr -> ExprComparer.Traverse visitor expr |> ignore)

        let marks =
            marks
            |> Seq.filter (fun item ->
                let firstGen, lastGen = item.Value
                firstGen <> lastGen)
            |> Seq.toArray

        let firstGen =
            marks
            |> Seq.map (fun item -> item.Value |> fst, item.Key)
            |> Seq.sortBy fst
            |> Seq.toList

        let lastGen =
            marks
            |> Seq.map (fun item -> item.Value |> snd, item.Key)
            |> Seq.sortBy fst
            |> Seq.toList

        let localExpr = Dictionary(marks.Length, exprComparer)

        let rec markToLocal' firstGen lastGen locals =
            match firstGen with
            | (gen, expr) :: firstGen ->
                let rec reclaim gen lastGen locals =
                    match lastGen with
                    | (gen', expr) :: lastGen when gen' < gen -> reclaim gen lastGen (localExpr[expr] :: locals)
                    | _ -> lastGen, locals

                let lastGen, locals = reclaim gen lastGen locals

                match locals with
                | local :: locals ->
                    localExpr.Add(expr, local)
                    markToLocal' firstGen lastGen locals
                | [] ->
                    localExpr.Add(expr, localExpr.Count)
                    markToLocal' firstGen lastGen locals
            | [] -> ()

        markToLocal' firstGen lastGen []

        let localLength =
            if localExpr.Count > 0 then
                (localExpr.Values |> Seq.max) + 1
            else
                0

        localExpr, localLength

    let findVariable variables v =
        match variables |> Array.tryFindIndex ((=) v) with
        | Some i -> i
        | None -> $"Variable not specified: {v}" |> invalidArg "variables"

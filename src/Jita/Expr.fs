// <copyright file="Expr.fs" company="Jita Project">
// Copyright (c) Jita Project. All rights reserved.
// </copyright>
namespace rec Jita

open System
open System.Collections.Generic
open System.Numerics
open Jita.Math
open Jita.Private

/// <summary>
/// Represents a function that takes two <see cref="Expr"/> instances.
/// </summary>
type BinaryFunction =
    /// <summary>
    /// Use a function that adds two <see cref="Expr"/> instances.
    /// </summary>
    | Add

    /// <summary>
    /// Use a function that takes the magnitude of the first <see cref="Expr"/> instance and the sign of the second
    /// <see cref="Expr"/> instance.
    /// </summary>
    | CopySign

    /// <summary>
    /// Use a function that divides two <see cref="Expr"/> instances.
    /// </summary>
    | Divide

    /// <summary>
    /// Use a function that returns the hypotenuse given two <see cref="Expr"/> instances that represent the lengths of
    /// the shorter sides of a right-angled triangle.
    /// </summary>
    | Hypotenuse

    /// <summary>
    /// Use a function that takes the maximum of two <see cref="Expr"/> instances.
    /// </summary>
    | Max

    /// <summary>
    /// Use a function that takes the minimum of two <see cref="Expr"/> instances.
    /// </summary>
    | Min

    /// <summary>
    /// Use a function that takes the modulus of a <see cref="Expr"/> instance with respect to another
    /// <see cref="Expr"/> function.
    /// </summary>
    | Modulus

    /// <summary>
    /// Use a function that multiplies two <see cref="Expr"/> instances.
    /// </summary>
    | Multiply

    /// <summary>
    /// Use a function that raises an <see cref="Expr"/> instance to the power of another <see cref="Expr"/> instance.
    /// </summary>
    | Power

    /// <summary>
    /// Use a function that subtracts an <see cref="Expr"/> instance to the power of another <see cref="Expr"/> instance.
    /// </summary>
    | Subtract

/// <summary>
/// Represents a function that takes a single <see cref="Expr"/> instance.
/// </summary>
type UnaryFunction =
    /// <summary>
    /// Use a function that returns the positive value of an <see cref="Expr"/> instance.
    /// </summary>
    | Absolute

    /// <summary>
    /// Use a function that returns the cube root of an <see cref="Expr"/> instance.
    /// </summary>
    | CubeRoot

    /// <summary>
    /// Use a function that raises e to the power of an <see cref="Expr"/> instance.
    /// </summary>
    | Exponent

    /// <summary>
    /// Use a function that takes the natural logarithm of an <see cref="Expr"/> instance.
    /// </summary>
    | Logarithm

    /// <summary>
    /// Use a function that negates an <see cref="Expr"/> instance.
    /// </summary>
    | Negate

    /// <summary>
    /// Use a function that takes the square root of an <see cref="Expr"/> instance.
    /// </summary>
    | SquareRoot

/// Represents a type that may be inverted.
type Invertible<'T> =
    /// An instance that is not inverted.
    | NonInverse of 'T

    /// An instance that is inverted.
    | Inverse of 'T

/// Indicates the type may be inverted.
type 'T invertible = Invertible<'T> // fsharplint:disable-line TypeNames

/// <summary>
/// Describes a visitor of <see cref="Expr"/> trees.
/// </summary>
type IExprVisitor<'T> =
    /// <summary>
    /// Visit a binary function (an <see cref="Expr.BinaryFunction"/> node).
    /// </summary>
    /// <param name="binaryFunction">The binary function being visited.</param>
    /// <param name="arg0">The first argument of the binary function.</param>
    /// <param name="arg1">The second argument of the binary function.</param>
    /// <returns>The result of applying the binary function to the arguments.</returns>
    abstract member VisitBinaryFunc: binaryFunction: BinaryFunction -> arg0: 'T -> arg1: 'T -> 'T

    /// <summary>
    /// Visit a constant (an <see cref="Expr"/>.<see cref="Expr.Constant"/> node).
    /// </summary>
    /// <param name="constant">The constant <see cref="Double"/>.</param>
    /// <returns>The constant converted to the target type.</returns>
    abstract member VisitConstant: constant: float -> 'T

    /// <summary>
    /// Visit the product of a list (an <see cref="Expr"/>.<see cref="Expr.Product"/> node).
    /// </summary>
    /// <param name="constant">The constant of the product.</param>
    /// <param name="args">The arguments to be multiplied.</param>
    /// <returns>The result of multiplying the constant and the arguments.</returns>
    abstract member VisitProduct: constant: float -> args: 'T Invertible list -> 'T

    /// <summary>
    /// Visit a n'th root (an <see cref="Expr"/>.<see cref="Expr.Root"/> node).
    /// </summary>
    /// <param name="value">The value whose n'th root is to be computed.</param>
    /// <param name="n">The degree of the root to be computed.</param>
    abstract member VisitRoot: value: 'T -> n: int -> 'T

    /// <summary>
    /// Visit the summing of a list (an <see cref="Expr"/>.<see cref="Expr.Sum"/> node).
    /// </summary>
    /// <param name="constant">The constant of the sum.</param>
    /// <param name="args">The arguments to be summed.</param>
    /// <returns>The result of summing the constant and the arguments.</returns>
    abstract member VisitSum: constant: float -> args: 'T Invertible list -> 'T

    /// <summary>
    /// Visit a unary function (an <see cref="Expr"/>.<see cref="Expr.UnaryFunction"/> node).
    /// </summary>
    /// <param name="unaryFunction">The unary function being visited.</param>
    /// <param name="arg">The argument of the unary function.</param>
    /// <returns>The result of applying the unary function to the argument.</returns>
    abstract member VisitUnaryFunc: unaryFunction: UnaryFunction -> arg: 'T -> 'T

    /// <summary>
    /// Visit a variable (an <see cref="Expr"/>.<see cref="Expr.Variable"/> node).
    /// </summary>
    /// <param name="variable">The variable.</param>
    /// <returns>The variable converted to the target type.</returns>
    abstract member VisitVariable: variable: string -> 'T

type IInlineExprVisitor<'T> =
    inherit IExprVisitor<'T>

    abstract member VisitInlineProduct: 'T -> 'T invertible -> 'T
    abstract member VisitInlineSum: 'T -> 'T invertible -> 'T

type private FunctionType =
    | ProductFunction
    | SumFunction

type private ExprLeaf =
    | Constant of float voption
    | Reference of int
    | Variable of string

type private ExprNode =
    | BinaryFunction of BinaryFunction * ExprLeaf * ExprLeaf
    | Product of float * ExprLeaf invertible list
    | Root of ExprLeaf * int
    | Sum of float * ExprLeaf invertible list
    | UnaryFunction of UnaryFunction * ExprLeaf

[<CustomEquality; CustomComparison>]
type Expr =
    | Annotation of Value: Expr * Expression: Expr
    | BinaryFunction of Function: BinaryFunction * Left: Expr * Right: Expr
    | Constant of Constant: float
    | Product of Constant: float * Expressions: Expr Invertible list
    | Root of Value: Expr * N: int
    | Sum of Constant: float * Expressions: Expr Invertible list
    | UnaryFunction of Function: UnaryFunction * Expression: Expr
    | Variable of Variable: string

    static member inline private mapConstantValue ([<InlineIfLambda>] mapper) value expr =
        match expr with
        | IsConstant value -> mapper value
        | _ -> value

    static member inline private mapConstantWith ([<InlineIfLambda>] mapper) defThunk expr =
        match expr with
        | IsConstant value -> mapper value
        | _ -> defThunk ()

    interface IAdditiveIdentity<Expr, Expr> with
        static member AdditiveIdentity = Expr.Zero

    interface IAdditionOperators<Expr, Expr, Expr> with
        static member (+)(left, right) = left + right

    interface IComparable with
        member this.CompareTo(other) =
            match other with
            | :? Expr as expr -> (this :> IComparable<Expr>).CompareTo(expr)
            | _ -> invalidArg (nameof other) $"Cannot compare {nameof Expr} against another type"

    interface IComparable<Expr> with
        member this.CompareTo(other) =
            match ExprComparer.Unwrap this, ExprComparer.Unwrap other with
            | Constant c1, Constant c2 -> compare c1 c2
            | Variable v1, Variable v2 -> compare v1 v2
            | _ -> invalidArg (nameof other) $"Cannot compare expressions: {this}, {other}"

    interface IComparisonOperators<Expr, Expr, bool> with
        static member op_LessThan(left, right) =
            (left :> IComparable<Expr>).CompareTo(right) < 0

        static member op_LessThanOrEqual(left, right) =
            (left :> IComparable<Expr>).CompareTo(right) <= 0

        static member op_GreaterThan(left, right) =
            (left :> IComparable<Expr>).CompareTo(right) > 0

        static member op_GreaterThanOrEqual(left, right) =
            (left :> IComparable<Expr>).CompareTo(right) >= 0

    interface IDecrementOperators<Expr> with
        static member op_Decrement(value) = value - Expr.One

    interface IDivisionOperators<Expr, Expr, Expr> with
        static member (/)(left, right) = left / right

    interface IEquatable<Expr> with
        member this.Equals(other) =
            (ExprComparer([| this; other |]) :> IEqualityComparer<Expr>).Equals(this, other)

    interface IEquatable<float> with
        member this.Equals(other) =
            match this with
            | Constant c -> c.Equals(other)
            | _ -> false

    interface IEquatable<string> with
        member this.Equals(other) =
            match this with
            | Variable v -> v.Equals(other)
            | _ -> false

    interface IEqualityOperators<Expr, Expr, bool> with
        static member op_Inequality(left, right) = not (left.Equals(right))
        static member op_Equality(left, right) = left.Equals(right)

    interface IFloatingPointConstants<Expr> with
        static member E = Expr.E

        static member Pi = Expr.Pi

        static member Tau = Expr.Tau

    interface IFormattable with
        member this.ToString(format, formatProvider) =
            match this with
            | IsConstant value -> value.ToString(format, formatProvider)
            | _ -> this.ToString()

    interface IIncrementOperators<Expr> with
        static member op_Increment(value) = value + Expr.One

    interface IModulusOperators<Expr, Expr, Expr> with
        static member (%)(left, right) = left % right

    interface IMultiplicativeIdentity<Expr, Expr> with
        static member MultiplicativeIdentity = Expr.One

    interface IMultiplyOperators<Expr, Expr, Expr> with
        static member (*)(left, right) = left * right

    interface INumber<Expr> with
        static member CopySign(value, sign) = Expr.CopySign value sign

        static member Max(x, y) = Expr.Max x y

        static member MaxNumber(x, y) = Expr.Max x y

        static member Min(x, y) = Expr.Min x y

        static member MinNumber(x, y) = Expr.Min x y

        static member Sign(value) =
            (value :> IComparable<Expr>).CompareTo(Constant 0.)

    interface INumberBase<Expr> with
        static member One = Expr.One
        static member Radix = 2
        static member Zero = Expr.Zero

        static member Abs(value) = abs value

        static member IsCanonical _ = true

        static member IsComplexNumber _ = false

        static member IsEvenInteger(value) =
            value |> Expr.mapConstantValue Double.IsEvenInteger false

        static member IsFinite(value) =
            value |> Expr.mapConstantValue Double.IsFinite false

        static member IsImaginaryNumber _ = false

        static member IsInfinity(value) =
            value |> Expr.mapConstantValue Double.IsInfinity false

        static member IsInteger(value) =
            value |> Expr.mapConstantValue Double.IsInteger false

        static member IsNaN(value) =
            value |> Expr.mapConstantValue Double.IsNaN false

        static member IsNegative(value) =
            (value :> IComparable).CompareTo(Constant 0.) < 0

        static member IsNegativeInfinity(value) =
            value |> Expr.mapConstantValue Double.IsNegativeInfinity false

        static member IsNormal(value) =
            value |> Expr.mapConstantValue Double.IsNormal false

        static member IsOddInteger(value) =
            value |> Expr.mapConstantValue Double.IsOddInteger false

        static member IsPositive(value) =
            (value :> IComparable).CompareTo(Constant 0.) > 0

        static member IsPositiveInfinity(value) =
            value |> Expr.mapConstantValue Double.IsPositiveInfinity false

        static member IsRealNumber(value) =
            value |> Expr.mapConstantValue Double.IsRealNumber false

        static member IsSubnormal(value) =
            value |> Expr.mapConstantValue Double.IsSubnormal false

        static member IsZero(value) =
            value |> Expr.mapConstantValue ((=) 0.) false

        static member MaxMagnitude(x, y) = Expr.Max x y

        static member MaxMagnitudeNumber(x, y) = Expr.Max x y

        static member MinMagnitude(x, y) = Expr.Min x y

        static member MinMagnitudeNumber(x, y) = Expr.Min x y

        static member Parse(s: ReadOnlySpan<char>, style, provider) : Expr =
            Constant(Double.Parse(s, style, provider))

        static member Parse(s: string, style, provider) : Expr =
            Constant(Double.Parse(s, style, provider))

        static member TryConvertFromChecked(value: 'TOther, result) =
            let mutable constant = nan

            if 'TOther.TryConvertToChecked(value, &constant) then
                result <- Constant constant
                true
            else
                false

        static member TryConvertFromSaturating(value: 'TOther, result) =
            let mutable constant = nan

            if 'TOther.TryConvertToSaturating(value, &constant) then
                result <- Constant constant
                true
            else
                false

        static member TryConvertFromTruncating(value: 'TOther, result) =
            let mutable constant = nan

            if 'TOther.TryConvertToTruncating(value, &constant) then
                result <- Constant constant
                true
            else
                false

        static member TryConvertToChecked(value, result: 'TOther byref) =
            match ExprComparer.Unwrap value with
            | Constant value -> 'TOther.TryConvertFromChecked(value, &result)
            | _ -> false

        static member TryConvertToSaturating(value, result: 'TOther byref) =
            match ExprComparer.Unwrap value with
            | Constant value -> 'TOther.TryConvertFromSaturating(value, &result)
            | _ -> false

        static member TryConvertToTruncating(value, result: 'TOther byref) =
            match ExprComparer.Unwrap value with
            | Constant value -> 'TOther.TryConvertFromTruncating(value, &result)
            | _ -> false

        static member TryParse(s: ReadOnlySpan<char>, style, provider, result: byref<Expr>) =
            let mutable value = nan

            if Double.TryParse(s, style, provider, &value) then
                result <- Constant value
                true
            else
                false

        static member TryParse(s: string, style, provider, result: byref<Expr>) =
            let mutable value = nan

            if Double.TryParse(s, style, provider, &value) then
                result <- Constant value
                true
            else
                false

    interface IParsable<Expr> with
        static member Parse(s, provider) = Constant(Double.Parse(s, provider))

        static member TryParse(s, provider, result) =
            let mutable value = nan

            if Double.TryParse(s, provider, &value) then
                result <- Constant value
                true
            else
                false

    interface IPowerFunctions<Expr> with
        static member Pow(x, y) = Expr.Pow(x, y)

    interface IRootFunctions<Expr> with
        static member Cbrt(x) = Expr.Cbrt x

        static member Hypot(x, y) = Expr.Hypot x y

        static member RootN(x, n) = Expr.RootN x n

        static member Sqrt(x) = Expr.Sqrt x

    interface ISignedNumber<Expr> with
        static member NegativeOne = Expr.NegativeOne

    interface ISpanFormattable with
        member this.TryFormat(destination, charsWritten, format, provider) =
            match this with
            | IsConstant value -> value.TryFormat(destination, &charsWritten, format, provider)
            | _ -> false

    interface ISpanParsable<Expr> with
        static member Parse(s, provider) = Constant(Double.Parse(s, provider))

        static member TryParse(s, provider, result) : bool =
            let mutable value = nan

            if Double.TryParse(s, provider, &value) then
                result <- Constant value
                true
            else
                false

    interface ISubtractionOperators<Expr, Expr, Expr> with
        static member (-)(left, right) = left - right

    interface IUnaryNegationOperators<Expr, Expr> with
        static member (~-) value = -value

    interface IUnaryPlusOperators<Expr, Expr> with
        static member (~+) value = value

    interface IUtf8SpanFormattable with
        member this.TryFormat(utf8Destination, bytesWritten, format, provider) =
            match this with
            | IsConstant value -> value.TryFormat(utf8Destination, &bytesWritten, format, provider)
            | _ -> false

    interface IUtf8SpanParsable<Expr> with
        static member Parse(s, provider) : Expr = Constant(Double.Parse(s, provider))

        static member TryParse(s, provider, result) : bool =
            let mutable value = nan

            if Double.TryParse(s, provider, &value) then
                result <- Constant value
                true
            else
                false

    static member E = Constant Double.E

    static member NaN = Constant nan

    static member NegativeOne = Constant -1.

    static member One = Constant 1.

    static member Pi = Constant Double.Pi

    static member Tau = Constant Double.Tau

    static member Zero = Constant 0.

    static member (+)(left, right) =
        match left, right with
        | BinaryAnnotation((v1, e1), (v2, e2)) -> Annotation(v1 + v2, Sum(0., [ NonInverse e2; NonInverse e1 ]))
        | Constant 0., e
        | e, Constant 0. -> e
        | Constant c1, Constant c2 -> Constant(c1 + c2)
        | Constant c1, Sum(c2, s)
        | Sum(c1, s), Constant c2 -> Sum(c1 + c2, s)
        | Sum(c1, s1), Sum(c2, s2) -> Sum(c1 + c2, s2 @ s1)
        | Constant c, e
        | e, Constant c -> Sum(c, [ NonInverse e ])
        | Sum(c, s), e -> Sum(c, (NonInverse e) :: s)
        | e, Sum(c, s) -> Sum(c, s @ [ NonInverse e ])
        | _, _ -> Sum(0., [ NonInverse right; NonInverse left ])

    static member (/)(dividend, divisor) =
        match dividend, divisor with
        | BinaryAnnotation((v1, e1), (v2, e2)) -> Annotation(v1 / v2, Product(1., [ Inverse e2; NonInverse e1 ]))
        | e, Constant 1. -> e
        | UnaryFunction(Negate, dividend), divisor
        | dividend, UnaryFunction(Negate, divisor) -> -(dividend / divisor)
        | Constant 0., _ -> dividend
        | Constant v1, Constant v2 -> Constant(v1 / v2)
        | Constant c1, Product(c2, s) -> Product(c1 / c2, s |> Operators.reciprocalAll [])
        | Product(c1, s), Constant c2 -> Product(c1 / c2, s)
        | Product(c1, s1), Product(c2, s2) -> Product(c1 / c2, (s2 |> Operators.reciprocalAll []) @ s1)
        | Constant c, e -> Product(c, [ Operators.reciprocal (NonInverse e) ])
        | e, Constant c -> Product(1. / c, [ NonInverse e ])
        | Product(c, s), e -> Product(c, (NonInverse e |> Operators.reciprocal) :: s)
        | e, Product(c, s) -> Product(1. / c, (s |> Operators.reciprocalAll []) @ [ NonInverse e ])
        | _, _ -> Product(1., [ Operators.reciprocal (NonInverse divisor); NonInverse dividend ])

    static member (*)(left, right) =
        match left, right with
        | BinaryAnnotation((v1, e1), (v2, e2)) -> Annotation(v1 * v2, Product(1., [ NonInverse e2; NonInverse e1 ]))
        | Constant 1., e
        | e, Constant 1. -> e
        | UnaryFunction(Negate, left), right
        | left, UnaryFunction(Negate, right) -> -(left * right)
        | Constant 0., _ -> left
        | _, Constant 0. -> right
        | Constant c1, Constant c2 -> Constant(c1 * c2)
        | Constant c1, Product(c2, s)
        | Product(c1, s), Constant c2 -> Product(c1 * c2, s)
        | Product(c1, s1), Product(c2, s2) -> Product(c1 * c2, s2 @ s1)
        | Constant c, e
        | e, Constant c -> Product(c, [ NonInverse e ])
        | Product(c, s), e -> Product(c, (NonInverse e) :: s)
        | e, Product(c, s) -> Product(c, s @ [ NonInverse e ])
        | _, _ -> Product(1., [ NonInverse right; NonInverse left ])

    static member (-)(minuend, subtrahend) =
        match minuend, subtrahend with
        | BinaryAnnotation((v1, e1), (v2, e2)) -> Annotation(v1 - v2, Sum(0., [ Inverse e2; NonInverse e1 ]))
        | e, Constant 0. -> e
        | Constant v1, Constant v2 -> Constant(v1 - v2)
        | Constant 0., Sum(c, s) -> Sum(-c, s |> List.map Operators.negate)
        | Constant 0., e -> -e
        | Constant c1, Sum(c2, s) -> Sum(c1 - c2, s |> List.map Operators.negate)
        | Sum(c1, s), Constant c2 -> Sum(c1 - c2, s)
        | Sum(c1, s1), Sum(c2, s2) -> Sum(c1 - c2, (s2 |> List.map Operators.negate) @ s1)
        | Constant c, e -> Sum(c, [ Operators.negate (NonInverse e) ])
        | e, Constant c -> Sum(-c, [ NonInverse e ])
        | Sum(c, s), e -> Sum(c, (NonInverse e |> Operators.negate) :: s)
        | e, Sum(c, s) -> Sum(-c, (s |> List.map Operators.negate) @ [ (NonInverse e) ])
        | _, _ -> Sum(0., [ Inverse subtrahend; NonInverse minuend ])

    static member (~-) value = Operators.negative value

    static member (%)(value, divisor) : Expr =
        match value, divisor with
        | BinaryAnnotation((v1, e1), (v2, e2)) -> Annotation(v1 % v2, BinaryFunction(Modulus, e1, e2))
        | _, Constant 0. -> Expr.NaN
        | Constant 0., _ -> value
        | Constant v1, Constant v2 -> Constant(v1 % v2)
        | _, _ -> BinaryFunction(Modulus, value, divisor)

    static member Abs value = Operators.apply abs Absolute value

    static member Cbrt value = Operators.apply cbrt CubeRoot value

    static member CopySign value sign =
        Operators.apply2 copysign CopySign (value, sign)

    static member Exp value = Operators.apply exp Exponent value

    static member Hypot x y =
        Operators.apply2 hypot Hypotenuse (x, y)

    static member Log value = Operators.apply log Logarithm value

    static member Max left right = Operators.apply2 max Max (left, right)

    static member Min left right = Operators.apply2 min Min (left, right)

    static member Pow(``base``, exponent) =
        match ``base``, exponent with
        | BinaryAnnotation((v1, e1), (v2, e2)) -> Annotation(v1 ** v2, BinaryFunction(Power, e1, e2))
        | _, Constant 0. -> Expr.One
        | Constant 1., _
        | Constant 0., _ -> ``base``
        | Constant b, Constant e -> Constant(b ** e)
        | _, _ -> BinaryFunction(Power, ``base``, exponent)

    static member RootN value n =
        match value, n with
        | Annotation(v, e), _ -> Annotation(Expr.RootN v n, Root(e, n))
        | _, 0 -> Expr.NaN
        | Constant 0., _ -> value
        | _, _ -> Root(value, n)

    static member Sqrt value = Operators.apply sqrt SquareRoot value

    static member op_Implicit value = Constant value

    static member op_Implicit value = Variable value

    static member op_Explicit expr =
        match expr with
        | IsConstant value -> value
        | expr -> raise (InvalidCastException $"Not a constant expression: %A{expr}")

    member this.TryGetConstant(constant: float outref) =
        match this with
        | IsConstant value ->
            constant <- value
            true
        | _ -> false

    override this.Equals(obj) =
        match obj with
        | :? Expr as other -> (this :> IEquatable<Expr>).Equals(other)
        | :? float as other -> (this :> IEquatable<float>).Equals(other)
        | :? string as other -> (this :> IEquatable<string>).Equals(other)
        | _ -> false

    override this.GetHashCode() =
        (ExprComparer([| this |]) :> IEqualityComparer<Expr>).GetHashCode(this)

type ControlledExpressionVisitor<'T> =
    { Callback: IExprVisitor<'T>
      LeadFilter: Expr -> 'T option
      TailFilter: Expr -> 'T -> 'T }

type private Result<'T> =
    | Callback of Expr * ('T -> Result<'T>)
    | Value of Expr * 'T

type ExprComparer([<ParamArray>] exprs) =
    static let (@>>) expr func = Callback(expr, func)

    static let rec unwrap =
        function
        | Annotation(expr, _) -> unwrap expr
        | expr -> expr

    static let (|Unwrap|) = unwrap

    static let traverseFuncN x (callback: IExprVisitor<'a>) =
        let bind product sum =
            function
            | ProductFunction -> product
            | SumFunction -> sum

        match callback with
        | :? IInlineExprVisitor<'a> as callback ->
            let visitInverse =
                x
                |> bind
                    (fun e -> callback.VisitBinaryFunc Divide (callback.VisitConstant 1.) e)
                    (callback.VisitUnaryFunc Negate)

            let visit = x |> bind callback.VisitInlineProduct callback.VisitInlineSum
            let absorb = x |> bind 1. 0.

            let traverseFuncN' expr (c, s) =
                let (!) value = Value(expr, value)
                let rec funcN' e1 s op e2 = result (visit e1 (op e2)) s

                and result e1 =
                    function
                    | NonInverse e2 :: s -> e2 @>> funcN' e1 s NonInverse
                    | Inverse e2 :: s -> e2 @>> funcN' e1 s Inverse
                    | [] -> !e1

                match s |> List.rev with
                | NonInverse e :: s when c = absorb -> e @>> fun e -> result e s
                | Inverse e :: s when c = absorb -> e @>> fun e -> result (visitInverse e) s
                | _ :: _ as s -> Constant c @>> fun c -> result c s
                | [] -> !(callback.VisitConstant c)

            traverseFuncN'
        | callback ->
            let visit = x |> bind callback.VisitProduct callback.VisitSum

            let traverseFuncN' expr (c, s) =
                let (!) value = Value(expr, value)
                let rec funcN' s' s op e = result ((op e) :: s') s

                and result s' =
                    function
                    | NonInverse e :: s -> e @>> funcN' s' s NonInverse
                    | Inverse e :: s -> e @>> funcN' s' s Inverse
                    | [] -> !(visit c s')

                s |> List.rev |> result []

            traverseFuncN'

    static let traverse (visitor: ControlledExpressionVisitor<'a>) expr =
        let funcProduct = traverseFuncN ProductFunction visitor.Callback
        let funcSum = traverseFuncN SumFunction visitor.Callback

        let rec traverse' exprs callbacks value =
            let inline callback' callbacks =
                function
                | Callback(expr, callback) -> traverse' (expr :: exprs) (callback :: callbacks) None
                | Value(expr, value) -> traverse' exprs callbacks (Some(visitor.TailFilter expr value))

            match value with
            | Some value ->
                match callbacks with
                | callback :: callbacks -> callback' callbacks (callback value)
                | [] -> value
            | None ->
                match exprs with
                | Unwrap expr :: exprs ->
                    match expr |> visitor.LeadFilter with
                    | Some value -> traverse' exprs callbacks (Some value)
                    | None ->
                        let (!) value = Value(expr, value)
                        let (>>!) e func = Callback(e, func >> (!))

                        match expr with
                        | BinaryFunction(op, e1, e2) -> e1 @>> (fun v1 -> e2 >>! visitor.Callback.VisitBinaryFunc op v1)
                        | Constant c -> !(visitor.Callback.VisitConstant c)
                        | Expr.Product(c, s) -> funcProduct expr (c, s)
                        | Root(e, n) -> e >>! (fun e -> visitor.Callback.VisitRoot e n)
                        | Expr.Sum(c, s) -> funcSum expr (c, s)
                        | UnaryFunction(op, e) -> e >>! visitor.Callback.VisitUnaryFunc op
                        | Variable name -> !(visitor.Callback.VisitVariable name)
                        | Annotation _ -> failwith "Unexpected expression after unwrapping"
                        |> callback' callbacks
                | _ -> invalidArg (nameof exprs) "Expression list must contain at least one item"

        traverse' [ expr ] [] None

    static let traverseCached (cache: IDictionary<Expr, 'a>) callback expr =
        let visitor =
            { Callback = callback
              LeadFilter =
                fun expr ->
                    match expr with
                    | BinaryFunction _
                    | Expr.Product _
                    | Root _
                    | Expr.Sum _
                    | UnaryFunction _ ->
                        match cache.TryGetValue(expr) with
                        | true, value -> Some value
                        | false, _ -> None
                    | Annotation _
                    | Constant _
                    | Variable _ -> None
              TailFilter =
                fun expr value ->
                    match expr with
                    | BinaryFunction _
                    | Expr.Product _
                    | Root _
                    | Expr.Sum _
                    | UnaryFunction _ ->
                        cache.Add(expr, value)
                        value
                    | Annotation _
                    | Constant _
                    | Variable _ -> value }

        traverse visitor expr

    static let traverseFast callback expr =
        traverseCached (Dictionary(PhysicalEqualityComparer())) callback expr

    let references =
        let references = Dictionary(PhysicalEqualityComparer())
        let exprLeaves = Dictionary()

        let inline toReference node =
            match exprLeaves.TryGetValue(node) with
            | true, ref -> ref
            | false, _ ->
                let ref = Reference(exprLeaves.Count + 1)
                exprLeaves.Add(node, ref)
                ref

        Array.iter
            (fun expr ->
                traverseCached
                    references
                    { new IExprVisitor<ExprLeaf> with
                        member _.VisitBinaryFunc op e1 e2 =
                            ExprNode.BinaryFunction(op, e1, e2) |> toReference

                        member _.VisitConstant c =
                            ExprLeaf.Constant(if Double.IsNaN(c) then ValueNone else ValueSome c)

                        member _.VisitProduct c s = ExprNode.Product(c, s) |> toReference

                        member _.VisitRoot e n = ExprNode.Root(e, n) |> toReference

                        member _.VisitSum c s = ExprNode.Sum(c, s) |> toReference

                        member _.VisitUnaryFunc op e =
                            ExprNode.UnaryFunction(op, e) |> toReference

                        member _.VisitVariable v = ExprLeaf.Variable v }
                    expr
                |> ignore)
            exprs

        references :> IReadOnlyDictionary<Expr, ExprLeaf>

    static member internal Traverse visitor expr = traverse visitor expr

    static member internal TraverseCached cache callback expr = traverseCached cache callback expr

    static member internal TraverseFast callback expr = traverseFast callback expr

    static member Unwrap expr = unwrap expr

    interface IEqualityComparer<Expr> with
        member _.Equals(x, y) =
            let x = unwrap x
            let y = unwrap y

            match x, y with
            | _, _ when x .=. y -> true
            | Constant c1, Constant c2 -> c1.Equals(c2)
            | Variable v1, Variable v2 -> v1 = v2
            | BinaryFunction _, BinaryFunction _
            | Product _, Product _
            | Root _, Root _
            | Sum _, Sum _
            | UnaryFunction _, UnaryFunction _ -> references[x] = references[y]
            | _ -> false

        member _.GetHashCode(expr) =
            match expr |> unwrap with
            | Constant c -> hash c
            | Variable v -> hash v
            | _ -> hash references[expr]

[<AutoOpen>]
module ExprMatching =
    let (|IsConstant|_|) expr =
        ExprComparer.Unwrap expr
        |> function
            | Constant c -> Some c
            | _ -> None

    let (|BinaryAnnotation|_|) =
        function
        | Annotation(v1, e1), Annotation(v2, e2) -> Some((v1, e1), (v2, e2))
        | e1, Annotation(v2, e2) -> Some((e1, e1), (v2, e2))
        | Annotation(v1, e1), e2 -> Some((v1, e1), (e2, e2))
        | _ -> None

module private Operators =
    let rec negate =
        function
        | NonInverse(Constant c) -> NonInverse(Constant -c)
        | NonInverse(Product(1., _) as e) -> NonInverse(UnaryFunction(Negate, e))
        | NonInverse(Product(c, s)) -> NonInverse(Product(-c, s))
        | NonInverse(Sum(c, s)) -> NonInverse(Sum(-c, s |> List.map negate))
        | NonInverse(UnaryFunction(Negate, e)) -> NonInverse e
        | NonInverse expr -> Inverse expr
        | Inverse expr -> NonInverse expr

    let rec negative expr =
        match expr with
        | Annotation(v, e) -> Annotation(negative v, UnaryFunction(Negate, e))
        | _ ->
            match NonInverse expr |> negate with
            | NonInverse e -> e
            | Inverse e -> UnaryFunction(Negate, e)

    [<TailCall>]
    let rec reciprocalAll s' =
        function
        | e :: s -> reciprocalAll ((e |> reciprocal) :: s') s
        | [] -> s'

    and reciprocal =
        function
        | NonInverse(BinaryFunction(Power, e1, e2)) -> NonInverse(BinaryFunction(Power, e1, negative e2))
        | NonInverse(Constant c) -> NonInverse(Constant(1. / c))
        | NonInverse(UnaryFunction(Exponent, e)) -> NonInverse(UnaryFunction(Exponent, negative e))
        | NonInverse e -> Inverse e
        | Inverse e -> NonInverse e

    let rec apply func op =
        function
        | Annotation(v, e) -> Annotation(apply func op v, UnaryFunction(op, e))
        | Constant c -> Constant(func c)
        | e -> UnaryFunction(op, e)

    [<TailCall>]
    let rec apply2 func op =
        function
        | BinaryAnnotation((v1, e1), (v2, e2)) -> Annotation(apply2 func op (v1, v2), BinaryFunction(op, e1, e2))
        | Constant c1, Constant c2 -> Constant(func c1 c2)
        | left, right -> BinaryFunction(op, left, right)
